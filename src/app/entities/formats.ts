import {User} from './user';

export class Formats {
  public static readonly USER_NAME_MIN_SIZE = 1;
  public static readonly FULL_NAME_MIN_SIZE = 1;
  public static readonly PASSWORD_MIN_SIZE = 1;
  public static readonly USER_NAME_MAX_SIZE = 20;
  public static readonly FULL_NAME_MAX_SIZE = 200;
  public static readonly USER_NAME_PATTERN = '^[a-z0-9]+$';
  public static readonly FULL_NAME_PATTERN = '^[a-zA-Z' +
    'àáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.\'-]+$';
  public static readonly PASSWORD_PATTERN = '^(?=.*[A-Z])(?=.*[0-9])';

}
