export class User {
  fullName: string;
  userName: string;
  password: string;
  lastUpdate: Date;

  constructor(fullName: string, userName: string, password: string, lastUpdate: Date) {
    this.fullName = fullName;
    this.password = password
    this.userName = userName;
    this.lastUpdate = lastUpdate;
  }
}
