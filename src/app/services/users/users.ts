import {User} from '../../entities/user';

export class Users {
  data: User[];

  constructor(data: User[]) {
    this.data = data;
  }
}
