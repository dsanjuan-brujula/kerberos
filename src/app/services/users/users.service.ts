import { Injectable } from '@angular/core';
import { User } from '../../entities/user';
import { Observable, of } from 'rxjs';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {catchError, retry, tap} from 'rxjs/internal/operators';
import {Users} from './users';

const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json'
  })
};

@Injectable({ providedIn: 'root' })
export class UsersService {

  constructor(private http: HttpClient) { }

  getUsers(): Observable<Users> {
    return this.http.get<Users>(environment.apiUrl + environment.usersUrl, httpOptions);
  }

  getUser(userName: string): Observable<User> {
    return this.http.get<User>(this.buildUserUrl(userName));
  }


  createUser(user: User): Observable<User> {
    return this.http.post<User>(environment.apiUrl + environment.usersUrl, user, httpOptions);
  }

  update(userName: string, user: User): Observable<User> {
    return this.http.patch<User>(this.buildUserUrl(userName), user, httpOptions);
  }

  buildUserUrl(userName: string) {
    return environment.apiUrl + environment.usersUrl + `/${userName}`;
  }
}
