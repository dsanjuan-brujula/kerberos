import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {passwordConfirmValidator} from '../input/input-password/password-confirm.validator';
import {User} from '../../entities/user';
import {UsersService} from '../../services/users/users.service';
import {PasswordFieldStateMatcher} from '../input/input-password/password-field-state-matcher';
import {ErrorStateMatcher} from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss']
})
export class NewUserComponent implements OnInit {
  userForm: FormGroup;
  hide: boolean;
  matcher: ErrorStateMatcher;

  constructor(private fb: FormBuilder, private userService: UsersService, private router: Router) {
  }

  ngOnInit() {
    this.matcher = new PasswordFieldStateMatcher();
    this.hide = true;
    this.createForm();
  }

  createUser(newUser: User): void {
    if (!newUser) { return; }
    this.userService.createUser(newUser).subscribe(user => {
      console.log(user);
      this.router.navigateByUrl('/users');
      });
  }

  createForm() {
    this.userForm = this.fb.group({
      userName: ['', [Validators.required, Validators.minLength(3)]],
      fullName: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(8), ]],
      confirmPassword: ['', Validators.required]
    }, {validators: passwordConfirmValidator});
  }
  get password() {
    return this.userForm.get('password');
  }

  get confirmPassword() {
    return this.userForm.get('confirmPassword');
  }

  get fullName() {
    return this.userForm.get('fullName');
  }

  get userName() {
    return this.userForm.get('userName');
  }

  onSubmit() { // TODO: Use EventEmitter with form value
    const newUser =  new User(this.userForm.value.fullName, this.userForm.value.userName, this.userForm.value.password, null);
    this.createUser(newUser);
  }


}
