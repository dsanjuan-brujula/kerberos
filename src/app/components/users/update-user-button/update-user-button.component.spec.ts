import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateUserButtonComponent } from './update-user-button.component';

describe('UpdateUserButtonComponent', () => {
  let component: UpdateUserButtonComponent;
  let fixture: ComponentFixture<UpdateUserButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateUserButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateUserButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
