import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {User} from '../../../entities/user';
import {UpdateUserDialogComponent} from '../../update-user/update-user-dialog.component';

@Component({
  selector: 'app-update-user-button',
  templateUrl: './update-user-button.component.html',
  styleUrls: ['./update-user-button.component.scss']
})
export class UpdateUserButtonComponent implements OnInit {

  @Input() user: User;
  @Output() valueChanged: EventEmitter<User>;

  constructor(public dialog: MatDialog) {
    this.valueChanged = new EventEmitter<User>();
  }

  ngOnInit() {}


  openDialog(user: User) {
    console.log(user);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title: 'Actualizar Usuario',
      userData: user
    };

    const dialogRef = this.dialog.open(UpdateUserDialogComponent, dialogConfig);

    const sub = dialogRef.componentInstance.valueChanged.subscribe({
      next: (userEvent: User) => {
        this.valueChanged.emit(userEvent as User);
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
}
