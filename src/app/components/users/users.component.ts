import {Component, OnInit, ViewChild} from '@angular/core';
import {UsersService} from '../../services/users/users.service';
import {User} from '../../entities/user';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  displayedColumns: string[] = ['userName', 'name', 'lastUpdate', 'edit'];
  users: MatTableDataSource<User> ;
  selectedUser: User;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private usersService: UsersService) { }

  getUsers(): void {
    this.usersService.getUsers()
      .subscribe(users => this.users = new MatTableDataSource(users.data));
  }

  onSelect(user: User): void {
    this.selectedUser = user;
  }

  applyFilter(filterValue: string) {
    this.users.filter = filterValue.trim().toLowerCase();

    if (this.users.paginator) {
      this.users.paginator.firstPage();
    }
  }

  ngOnInit() {
    this.getUsers();
  }
}
