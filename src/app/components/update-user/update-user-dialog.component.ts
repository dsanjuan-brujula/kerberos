import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ErrorStateMatcher, MatDialog} from '@angular/material';
import {UsersService} from '../../services/users/users.service';
import {PasswordFieldStateMatcher} from '../input/input-password/password-field-state-matcher';
import {passwordConfirmValidator} from '../input/input-password/password-confirm.validator';
import {User} from '../../entities/user';
import {MAT_DIALOG_DATA} from '@angular/material';


@Component({
  selector: 'app-update-user-dialog',
  templateUrl: './update-user-dialog.component.html',
  styleUrls: ['./update-user-dialog.component.scss']
})
export class UpdateUserDialogComponent implements OnInit {

  userForm: FormGroup;
  hide: boolean;
  matcher: ErrorStateMatcher;

  @Output() valueChanged: EventEmitter<User>;

  constructor(private fb: FormBuilder, private userService: UsersService, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.valueChanged = new EventEmitter<User>();
  }

  ngOnInit() {
    this.matcher = new PasswordFieldStateMatcher();
    this.hide = true;
    this.createForm();
  }

  // TODO try to move to each component
  createForm() {
    this.userForm = this.fb.group({
      fullName: [this.data.userData.fullName, [Validators.required]],
      password: ['', [Validators.minLength(8)]],
      confirmPassword: ['']
    }, {validators: passwordConfirmValidator});
  }

  get password() {
    return this.userForm.get('password');
  }

  get confirmPassword() {
    return this.userForm.get('confirmPassword');
  }

  get fullName() {
    return this.userForm.get('fullName');
  }

  get userName() {
    return this.userForm.get('userName');
  }

  onSubmit() {
    const updated = new User(this.userForm.value.fullName, null, this.userForm.value.password, null);
    console.warn(updated);
    this.updateUser(this.data.userData.userName, updated);
  }

  private updateUser(userName: string, user: User) {
    const data = {
      fullName: user.fullName,
      password: (user.password == null || user.password === undefined || user.password === '') ? null : user.password
    }
    this.userService.update(userName, data as User).subscribe((updatedUser) => {
      console.log(updatedUser);
      this.valueChanged.emit(updatedUser);
    });
  }
}
