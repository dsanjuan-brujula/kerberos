import {Component, Input, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material';

@Component({
  selector: 'app-input-full-name',
  templateUrl: './input-full-name.component.html',
  styleUrls: ['./input-full-name.component.scss']
})
export class InputFullNameComponent implements OnInit {

  @Input() fullNameControl: FormControl;
  @Input() fullName: string;

  constructor() { }

  ngOnInit() {
  }
}
