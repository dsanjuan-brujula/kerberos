import {Component, Input, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-input-user-name',
  templateUrl: './input-user-name.component.html',
  styleUrls: ['./input-user-name.component.scss']
})
export class InputUserNameComponent implements OnInit {

  @Input() userNameControl: FormControl;

  constructor() { }

  ngOnInit() {
  }

}
