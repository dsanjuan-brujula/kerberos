import {FormControl, FormGroupDirective, NgForm} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material';

export class PasswordFieldStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return (control && (control.invalid || form.hasError('passwordsDoNotMatch')) && (control.dirty || control.touched ));
  }
}
