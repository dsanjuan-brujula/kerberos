import { Component, OnInit, Input } from '@angular/core';
import {FormControl} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material';

@Component({
  selector: 'app-input-password',
  templateUrl: './input-password.component.html',
  styleUrls: ['./input-password.component.scss']
})
export class InputPasswordComponent implements OnInit {

  hide: boolean;

  @Input() passFormControl: FormControl;
  @Input() confPassFormControl: FormControl;
  @Input() errorStateMatcher: ErrorStateMatcher;

  constructor() { }

  ngOnInit() {
    this.hide = true;
  }

}
