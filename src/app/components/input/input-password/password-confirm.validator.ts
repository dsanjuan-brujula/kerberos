import {FormGroup, ValidationErrors, ValidatorFn} from '@angular/forms';

export const passwordConfirmValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const pass = control.get('password');
  const passConfirm = control.get('confirmPassword');

  return pass && passConfirm && pass.value !== passConfirm.value ? { 'passwordsDoNotMatch': true } : null;
};
