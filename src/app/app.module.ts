import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms'; // <-- NgModel lives here

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app.component';
import { UsersComponent } from './components/users/users.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatInputModule, MatPaginatorModule, MatTableModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule,
  MatListModule, MatFormFieldModule, MatOptionModule, MatSelectModule, MatDialogContent, MatDialogModule
} from '@angular/material';
import { NotFoundComponent } from './components/shared/not-found/not-found.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MainNavComponent } from './components/main-nav/main-nav.component';
import { NewUserComponent } from './components/create-user/new-user.component';
import {HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { InputPasswordComponent } from './components/input/input-password/input-password.component';
import { InputFullNameComponent } from './components/input/input-full-name/input-full-name.component';
import { InputUserNameComponent } from './components/input/input-user-name/input-user-name.component';
import { UpdateUserDialogComponent } from './components/update-user/update-user-dialog.component';
import { UpdateUserButtonComponent } from './components/users/update-user-button/update-user-button.component';
import {HttpErrorInterceptor} from './interceptor/http-error.interceptor';
import { LoaderComponent } from './components/shared/loader/loader.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    NotFoundComponent,
    MainNavComponent,
    NewUserComponent,
    InputPasswordComponent,
    InputFullNameComponent,
    InputUserNameComponent,
    UpdateUserDialogComponent,
    UpdateUserButtonComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatProgressSpinnerModule,
    FormsModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatInputModule,
    MatPaginatorModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatDialogModule,
    HttpClientModule
  ],
  providers: [UpdateUserDialogComponent, {
    provide: HTTP_INTERCEPTORS,
    useClass: HttpErrorInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent],
  entryComponents: [UpdateUserDialogComponent]
})
export class AppModule {
}
