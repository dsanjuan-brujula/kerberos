export const environment = {
  production: true,
  usersUrl: '/users',
  apiUrl: 'http://localhost:8080/kerberos-api/v1'
};
